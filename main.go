package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {
	var (
		// From environment
		port         = envInt("PORT")
		redirectCode = envInt("REDIRECT_CODE")
		targetDomain = os.Getenv("REDIRECT_DOMAIN")
		debug, _     = strconv.ParseBool(os.Getenv("DEBUG"))

		// Computed
		addr     = fmt.Sprintf(":%d", port)
		debugLog *log.Logger

		// Defaults
		targetScheme = "https"
	)

	if debug {
		debugLog = log.New(os.Stdout, "[DEBUG] ", 0)
	}

	flag.StringVar(&addr, "addr", addr, "Address to listen on")
	flag.IntVar(&redirectCode, "redirect-code", redirectCode, "Code to send redirects with (must be 3XX)")
	flag.StringVar(&targetDomain, "target-domain", targetDomain, "Domain to replace the one on each request")
	flag.StringVar(&targetScheme, "target-scheme", targetScheme, "Scheme to be used when redirecting")

	flag.Parse()

	if targetScheme == "" || targetDomain == "" || redirectCode == 0 {
		log.Fatalf("All options **must** be specified if not set on environment")
	}

	log.Printf("Starting server on %q", addr)

	e := http.ListenAndServe(addr, http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		locationURL := *req.URL
		locationURL.Scheme = targetScheme
		locationURL.Host = targetDomain

		if debugLog != nil {
			debugLog.Printf("%q -> %q (code %d)", req.URL.String(), locationURL.String(), redirectCode)
		}

		http.Redirect(rw, req, locationURL.String(), redirectCode)
	}))

	if e != nil && !errors.Is(e, http.ErrServerClosed) {
		log.Fatalf("Error on server: %s", e)
	}
}

func envInt(name string) int {
	c, _ := strconv.Atoi(os.Getenv(name))

	return c
}
