FROM docker.io/golang:alpine AS builder
ADD . code/
WORKDIR code
RUN go build -v -tags netgo -o /domain-redirector .

FROM scratch
COPY --from=builder /domain-redirector /redirector
ENTRYPOINT ["/redirector"]
